<?php
    return [
    
        'validation_error' => 'Validation error',
        'user_registered_successfully' => 'User registered successfully',
        'unauthorized' => 'Unauthorized',
        'user_login_successfully' => 'User login successfully',
        'created_successfully' => 'Created successfully',
        'not_found' => 'Not found',
        'retrieved_successfully' => 'Retrieved successfully',
        'updated_successfully' => 'Updated successfully',
        'deleted_successfully' => 'Deleted successfully',
    ];