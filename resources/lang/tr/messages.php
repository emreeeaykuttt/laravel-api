<?php
    return [
    
        'validation_error' => 'Doğrulama hatası',
        'user_registered_successfully' => 'Kullanıcı başarıyla kayıt oldu',
        'unauthorized' => 'Yetkiniz yok',
        'user_login_successfully' => 'Kullanıcı girişi başarıyla yapıldı',
        'created_successfully' => 'Başarıyla oluşturuldu',
        'not_found' => 'Bulunamadı',
        'retrieved_successfully' => 'Başarıyla alındı',
        'updated_successfully' => 'Başarıyla güncellendi',
        'deleted_successfully' => 'Başarıyla silindi',
    ];