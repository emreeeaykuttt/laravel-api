<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\SliderController;
use App\Http\Controllers\API\PageController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('localization')->group(function() {

    Route::middleware('auth:api')->group(function() {
        Route::get('profile', [AuthController::class, 'profile']);
        Route::resource('sliders', SliderController::class);
        Route::resource('pages', PageController::class);
    });

    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    Route::resource('sliders', SliderController::class, ['only' => ['index', 'show']]);
    Route::resource('pages', PageController::class, ['only' => ['index', 'show']]);

});
