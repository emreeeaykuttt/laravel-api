<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Slider;
use Validator;
use App\Http\Resources\Slider as SliderResource;

class SliderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->has('limit') ? $request->limit : 15;
        $sortby = $request->sortby;
        $direction = $request->direction;

        // $sliders = Slider::find(1);
        $sliders = Slider::select('*');

        if (!empty($sliders)) {

            if ($request->has('search')) {
                $sliders = $sliders->where('title', 'like', '%' .$request->search . '%');
                $sliders = $sliders->orWhere('detail', 'like', '%'. $request->search .'%');
            }

            if ($request->has('title')) {
                $sliders = $sliders->where('title', $request->title);
            }

            if($sortby && $direction){
                $sliders = $sliders->orderBy($sortby, $direction)->paginate($limit);
                $sliders->appends(['sortby' => $sortby, 'direction' => $direction])->links();
            } else {
                $sliders = $sliders->paginate($limit);
            }

            $sliders->appends(['limit' => $limit])->links();

            return SliderResource::collection($sliders);
        } else {
            return false;
        }
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'title' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError(trans('messages.validation_error'), $validator->errors());       
        }
   
        $slider = Slider::create([
            'title' => $request->title,
            'detail' => $request->detail,
            'link' => !empty($request->link) ? $request->link : '',
        ]);
   
        return $this->sendResponse(new SliderResource($slider), trans('messages.created_successfully'));
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slider = Slider::find($id);
  
        if (is_null($slider)) {
            return $this->sendError(trans('messages.not_found'));
        }
   
        return $this->sendResponse(new SliderResource($slider), trans('messages.retrieved_successfully'), 201);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'title' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError(trans('messages.validation_error'), $validator->errors());       
        }
   
        $slider->title = $input['title'];
        $slider->detail = $input['detail'];
        $slider->link = !empty($input['link']) ? $input['link'] : '';
        $slider->save();
   
        return $this->sendResponse(new SliderResource($slider), trans('messages.updated_successfully'));
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $slider->delete();
   
        return $this->sendResponse([], trans('messages.deleted_successfully'), 204);
    }
}
