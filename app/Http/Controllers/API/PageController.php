<?php

namespace App\Http\Controllers\API;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Resources\Page as PageResource;
use Validator;

class PageController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $limit = $request->has('limit') ? $request->limit : 15;
        $sortby = $request->sortby;
        $direction = $request->direction;
        $tranlation_model = $request->hasHeader('X-localization') ? 'pages' : 'pages_all_lang';

        $pages = Page::whereHas($tranlation_model, function($q) use ($request) {

            if ($request->has('search')) {
                $q->where(function($q) use ($request){
                    $q->where('title', 'like', '%' .$request->search . '%');
                    $q->orwhere('link', 'like', '%' .$request->search . '%');
                    $q->orwhere('description', 'like', '%' .$request->search . '%');
                    $q->orWhere('detail', 'like', '%'. $request->search .'%');
                });
            }
            
            if ($request->has('parent_id')) {
                $q->where('parent_id', $request->parent_id);
            }
            
        })->with($tranlation_model);

        if($sortby && $direction){
            $pages = $pages->orderBy($sortby, $direction)->paginate($limit);
            $pages->appends(['sortby' => $sortby, 'direction' => $direction])->links();
        } else {
            $pages = $pages->paginate($limit);
        }

        $pages->appends(['limit' => $limit])->links();

        return PageResource::collection($pages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }
}
