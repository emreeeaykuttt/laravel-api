<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:6',
            'c_password' => 'required|same:password',
        ]);
   
        if($validator->fails()){
            return $this->sendError(trans('messages.validation_error'), $validator->errors());       
        }
   
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['name'] = $user->name;
        $token_arr = $user->createToken('Laravel8PassportAuth');
        $success['token'] = $token_arr->accessToken; 
        $success['expires_at'] = strtotime($token_arr->token->expires_at);
   
        return $this->sendResponse($success, trans('messages.user_registered_successfully'));
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $success['name'] = $user->name;
            $token_arr = $user->createToken('Laravel8PassportAuth');
            $success['token'] = $token_arr->accessToken; 
            $success['expires_at'] = strtotime($token_arr->token->expires_at);
   
            return $this->sendResponse($success, trans('messages.user_login_successfully'));
        } 
        else{ 
            return $this->sendError(trans('messages.unauthorized'), ['error' => trans('messages.unauthorized')]);
        } 
    }

    public function profile() 
    {
        $user = auth()->user();
        return response()->json(['user' => $user], 200);
    }
}
