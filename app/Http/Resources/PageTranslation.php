<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PageTranslation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'title' => $this->title,
            'link' => $this->link,
            'referral_link' => $this->referral_link,
            'description' => $this->description,
            'detail' => $this->detail,
            'photo' => $this->photo,
            'sort' => $this->sort,
            'published' => $this->published,
            'language' => $this->language,
            'page_id' => $this->page_id,
        ];
    }
}
