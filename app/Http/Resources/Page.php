<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Page extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'pages' => PageTranslation::collection($this->whenLoaded('pages')),
            'pages_all_lang' => PageTranslation::collection($this->whenLoaded('pages_all_lang')),
        ];
    }
}
