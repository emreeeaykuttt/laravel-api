<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $hidden = ['created_at', 'updated_at'];

    public function pages()
    {
        return $this->hasMany(PageTranslation::class)->where('language', app()->getLocale());
    }
    
    public function pages_all_lang()
    {
        return $this->hasMany(PageTranslation::class);
    }
}
